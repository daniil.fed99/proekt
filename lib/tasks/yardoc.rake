begin
  require 'yard'

  YARD::Rake::YardocTask.new do |t|
    files = ['app/**/*.rb']
    files << Dir['lib/**/*.rb', 'plugins/**/*.rb'].reject {|f| f.match(/test/) }
    t.files = files

    static_files = ['doc1/CHANGELOG',
                    'doc1/COPYING',
                    'doc1/INSTALL',
                    'doc1/RUNNING_TESTS',
                    'doc1/UPGRADING'].join(',')

    t.options += ['--no-private', '--output-dir', './doc1/app', '--files', static_files]
  end

rescue LoadError
  # yard not installed (gem install yard)
  # http://yardoc.org
end
