from create_bot import dp, bot, base, cursor
from aiogram import Dispatcher
from aiogram.types import Message, CallbackQuery, InputFile, ReplyKeyboardMarkup, KeyboardButton
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
import sqlite3
from sql_request import dict_list, requestsText, changeText, rollbackText, insertPhoto, updatePhoto, choseCurrentPhoto
import keyboard

# --------------------------АДМИНКА--------------------------------------------------------------------




#for i in cursor.execute('SELECT text FROM text_message WHERE name_paragraph = "Что делать с телом?"').fetchall():
    #await message.answer(formatter(i[0]))

#admin_list = [464085688]

FORMAT_CODE_ADMIN = 'XX-XX-XX'




class Admin(StatesGroup):
    chose = State()
    photo_or_text = State()
    admin_state = State()
    code_state = State()
    text = State()
    photo = State()
    chose_photo = State()
    no_photo = State()


@dp.message_handler(commands=['admin'], state="*")
async def check_admin(message: Message, state: FSMContext):
    admin_code = []
    for code in cursor.execute('SELECT id FROM administrator_id').fetchall():
        admin_code.append(code[0])
    if message.chat.id not in admin_code:
        await message.answer('У вас нет привелегий администратора.\n\n'
                             'Чтобы получить права администратора нужен специальный код.\n\n Желаете получить права администратора?',
                             reply_markup=keyboard.yes_no_admin)
        async with state.proxy() as data:
            data["chat"] = message.chat.id
            data["message"] = message.message_id
        await Admin.admin_state.set()

    else:
        await message.answer('Что изменим?', reply_markup=keyboard.keyboard_photo_or_text)
        await Admin.photo_or_text.set()



@dp.callback_query_handler(lambda c: c.data and c.data.startswith('admin'), state=Admin.admin_state)
async def admin(query: CallbackQuery, state: FSMContext):
    if query.data[-1] == '1':
        await query.message.edit_text('Введите код в следующем формате: 11-22-33')
        await Admin.code_state.set()
    if query.data[-1] == '2':
        await query.message.edit_text('Всего доброго!')
        await state.finish()


@dp.message_handler(state=Admin.code_state)
async def what_chose(message: Message, state: FSMContext):
    code_admin = []
    for code in cursor.execute(f"SELECT code_get_admin from code").fetchall():
        code_admin.append(code[0])
    if len(FORMAT_CODE_ADMIN) != len(message.text):
        await message.answer('Неверный формат. Попробуйте еще раз!')
    elif message.text not in code_admin:
        await message.answer('Неверный код! Попробуйте еще раз!')
    else:
        cursor.execute(f"INSERT INTO administrator_id VALUES ({message.chat.id})")
        base.commit()
        await message.answer('Вы получили права администратора. Введите /admin для использования прав')


@dp.message_handler(text=['Текст', 'Картинки'], state=Admin.photo_or_text)
async def check_photo_or_text(message: Message, state: FSMContext):
    async with state.proxy() as data:
        data['current'] = message.text
    if message.text == 'Текст':
        await message.answer('Какой пункт изменим?', reply_markup=keyboard.keyboard_pravki)
        await Admin.chose.set()
    if message.text == 'Картинки':
        await message.answer('В каком пункте меняем картинки', reply_markup=keyboard.chose_photo)
        await Admin.chose.set()




@dp.message_handler(state=Admin.chose)
async def what_chose(message: Message, state: FSMContext):
    async with state.proxy() as data:
        current = data['current']
        data['paragraph'] = message.text
    if current == 'Текст':
        await message.answer('Напишите текст')
        async with state.proxy() as data:
            data['wc'] = message.text
        await Admin.text.set()
    if current == 'Картинки':
        current_chose_photo = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        list_dict_current_photo = choseCurrentPhoto(message.text)
        for ldcp in range(0, len(list_dict_current_photo)):
            current_chose_photo.row(KeyboardButton(list_dict_current_photo[ldcp]))
        current_chose_photo.row(KeyboardButton('Такой нет'))
        await message.answer('Выберите конкретную картинку', reply_markup=current_chose_photo)
        await Admin.chose_photo.set()


@dp.message_handler(state=Admin.text)
async def change_text(message: Message, state: FSMContext):
    async with state.proxy() as data:
        msg = data['wc']
    changeText(msg, message.text)
    await message.answer('Готово!')
    await state.finish()


@dp.message_handler(state=Admin.chose_photo)
async def change_text(message: Message, state: FSMContext):
    if message.text == 'Такой нет':
        await message.answer('Введите название для картинки')
        await Admin.no_photo.set()
    else:
        async with state.proxy() as data:
            data['current_photo'] = message.text
        await message.answer('Загрузите картинку')
        await Admin.photo.set()


@dp.message_handler(state=Admin.no_photo)
async def change_text(message: Message, state: FSMContext):
    async with state.proxy() as data:
        prg = data['paragraph']

    insertPhoto(message.photo[0].file_id, prg, message.text)
    await message.answer('Картинка добавлена!')
    await state.finish()


@dp.message_handler(content_types=['photo'], state=Admin.photo)
async def change_text(message: Message, state: FSMContext):
    async with state.proxy() as data:
        prg = data['paragraph']
        crnt = data['current_photo']

    updatePhoto(message.photo[0].file_id, prg, crnt)
    await message.answer('Готово! Если нужны еще исправления нажмите сюда -> /admin')
    await state.finish()







