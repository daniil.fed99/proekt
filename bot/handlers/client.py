import aiogram.types
from aiogram import types, Dispatcher

from create_bot import dp, bot
from aiogram.types import Message, CallbackQuery, InputFile
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from sql_request import dict_list, requestsText, haveOrNot, takePhoto
import keyboard


class City(StatesGroup):
    city = State()
    doc = State()
    body = State()
    morg = State()
    doc_morg = State()
    registration = State()
    chose = State()
    poxoron = State()


#@dp.message_handler(commands=['start'], state='*')
async def choose_city(message: Message, state: FSMContext):
    await message.answer('Здравствуйте! \n\n'
                         'Мы поможем вам в трудную минуту и расскажем какие шаги необходимо преодолеть!\n\n'
                         'Мы дадим информацию что необходимо сделать:\n'
                         '-В первые часы\n'
                         '-Что делать с телом\n'
                         '-Что делают в морге\n'
                         '-Регистрация смерти в ЗАГС\n')
    await message.answer('Чтобы получить точную информацию, выберите, пожалуйста, свой город.',
                         reply_markup=keyboard.choose_city)
    async with state.proxy() as data:
        data["id"] = message.chat.id
    await City.city.set()


async def info(id_chat):
    await bot.send_message(id_chat, 'Первые часы\n\n'
                                    'В первую очередь вам нужно вызвать <b>скорую помощь</b>, чтобы констатировать смерть, '
                                    'и полицию, чтобы провести осмотр тела. Это можно сделать по телефонам:\n\n'
                                    '112: Единый номер экстренных служб\n'
                                    '103: Скорая помощь\n'
                                    '102: Полиция\n\n'
                                    'Вам должны выбрать два документа:', reply_markup=keyboard.show_document)


#@dp.callback_query_handler(lambda c: c.data and c.data.startswith('city'), state=City.city)
async def procces_choose_city(query: CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        chat_id = data["id"]
    if 'next' in query.data:
        await info(chat_id)
        await City.doc.set()


#@dp.message_handler(text=['Показать документы'], state=City.doc)
async def process_view_doc(message: types.Message, state: FSMContext):
    await message.answer(requestsText('Показать документы'))
    flag = haveOrNot(message.text)
    if flag:
        await message.answer('Бланк констотации смерти')
        photo = InputFile(f"doc.jpg", 'rb')
        await bot.send_photo(chat_id=message.chat.id, photo=photo, reply_markup=None)

        await message.answer('Протокол осмотра тела')
        photo = InputFile("doc.jpg", 'rb')
        await bot.send_photo(chat_id=message.chat.id, photo=photo, reply_markup=keyboard.show_body)
        await City.body.set()
    else:
        dict_lst = takePhoto('Показать документы')
        for i in dict_lst.items():
            await message.answer(i[0])
            await bot.send_photo(chat_id=message.chat.id, photo=i[1], reply_markup=keyboard.show_body)
        await City.body.set()


#@dp.message_handler(text=['Что делать с телом?'], state=City.body)
async def process_help_command(message: types.Message, state: FSMContext):
    await message.answer(requestsText('Что делать с телом?'),
                         reply_markup=keyboard.show_morg)
    await City.morg.set()


# -------------------Тут уже второй шаг------------


#@dp.message_handler(text=['Морг'], state=City.morg)
async def process_morg(message: types.Message, state: FSMContext):
    await message.answer(requestsText('Морг'),
                         reply_markup=keyboard.show_doc_morg)
    await City.doc_morg.set()


#@dp.message_handler(text=['Документы в морге'], state=City.doc_morg)
async def process_doc_morg(message: types.Message, state: FSMContext):
    await message.answer('Медицинское свидетельство о смерти')
    photo = InputFile("med-doc.jpg", 'rb')
    await bot.send_photo(chat_id=message.chat.id, photo=photo, reply_markup=None)
    await message.answer('Бланк констатации смерти')
    photo = InputFile("med-doc.jpg", 'rb')
    await bot.send_photo(chat_id=message.chat.id, photo=photo, reply_markup=keyboard.show_registration)

    await City.registration.set()


# -------------------Тут уже третий шаг------------
#@dp.message_handler(text=['Регистрация в ЗАГС'], state=City.registration)
async def process_zags(message: types.Message, state: FSMContext):
    await message.answer(
        requestsText('Регистрация в ЗАГС'),
        reply_markup=keyboard.show_chose_references_or_next)

    await City.chose.set()


#@dp.message_handler(text=['Справка №11'], state=City.chose)
async def process_spravka(message: types.Message, state: FSMContext):
    await message.answer('Справка №11')
    photo = InputFile("D:/PyCharm Community Edition 2021.1.3/rituals_bot/form-11.jpg", 'rb')
    await bot.send_photo(chat_id=message.chat.id, photo=photo, reply_markup=keyboard.show_poxoron)
    #await City.poxoron.set()


#@dp.message_handler(text=['Похороны'], state=City.chose)
async def process_poxoron(message: types.Message, state: FSMContext):
    await message.answer(requestsText('Похороны'))

    await message.answer('Для организации похорон вы можете найти агента на нашем сайте rituals.com\n'
                         'Агент поможет собрать все необходимые докумернты, организовать поминки, купить все необходимые предметы погребения\n\n'
                         'Так же купить предметы погребения вы можете на сайте rituals.com')


def registerHandlerCliens(dp: Dispatcher):
    dp.register_message_handler(choose_city, commands=['start'], state='*')
    dp.register_callback_query_handler(procces_choose_city, state=City.city)
    dp.register_message_handler(process_view_doc, lambda msg: msg.text == 'Показать документы', state=City.doc)
    dp.register_message_handler(process_help_command, lambda msg: msg.text == 'Что делать с телом?', state=City.body)
    dp.register_message_handler(process_morg, lambda msg: msg.text == 'Морг', state=City.morg)
    dp.register_message_handler(process_doc_morg, lambda msg: msg.text =='Документы в морге', state=City.doc_morg)
    dp.register_message_handler(process_zags, lambda msg: msg.text =='Регистрация в ЗАГС', state=City.registration)
    dp.register_message_handler(process_spravka, lambda msg: msg.text =='Справка №11', state=City.chose)
    dp.register_message_handler(process_poxoron, lambda msg: msg.text =='Похороны', state=City.chose)


