from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.types import ReplyKeyboardMarkup, ReplyKeyboardRemove, KeyboardButton, InlineKeyboardMarkup, \
    InlineKeyboardButton
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import Message, CallbackQuery
from bot_tg import dp
from sql_request import takeKeys, takeKeysPhoto, choseCurrentPhoto
from create_bot import base, cursor

# Выбор города
#choose_city = InlineKeyboardMarkup()
#list_city = ['Москва', 'Питер', 'Ярославль', 'Екб']
#for l in range(0, len(list_city)):
    #choose_city.add(InlineKeyboardButton(list_city[l], callback_data=f'city_{l}'))

moskow = InlineKeyboardButton('Дальше', callback_data='next')
rostov_na_dony = InlineKeyboardButton('Ростов-на-Дону', callback_data='city_rostov_na_dony')
yekaterinburg = InlineKeyboardButton('Екатеринбург', callback_data='city_yekaterinburg')

choose_city = InlineKeyboardMarkup().row(moskow)

#Document
document = KeyboardButton('Показать документы')

show_document = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).add(document)

#Тело в морг
body = KeyboardButton('Что делать с телом?')

show_body = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).add(body)

#Морг
morg = KeyboardButton('Морг')

show_morg = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).add(morg)

#Документы в морге
doc_morg = KeyboardButton('Документы в морге')

show_doc_morg = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).add(doc_morg)

#Регистрация
registration = KeyboardButton('Регистрация в ЗАГС')

show_registration = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).add(registration)

#Справка по форме 11 или дальше
references = KeyboardButton('Справка №11')
next = KeyboardButton('Похороны')

show_chose_references_or_next = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).add(references).add(next)
    # Отдельно кнопка для перехода к пункту "Похороны" из спарки

poxoron = KeyboardButton('Похороны')
show_poxoron = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).add(poxoron)


#---Для правок
keyboard_photo_or_text = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
keyboard_photo_or_text.add(KeyboardButton('Текст'), KeyboardButton('Картинки'))


keyboard_pravki = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
list_dict = takeKeys()

for l in range(0, len(list_dict)):
    keyboard_pravki.row(KeyboardButton(list_dict[l]))

    #-----Картинки


#---Выбор картинки
chose_photo = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
list_dict_photo = takeKeysPhoto()
for ll in range(0, len(list_dict_photo)):
    chose_photo.row(KeyboardButton(list_dict_photo[ll]))


#---Выбор подкартинки



#----Спрашиваем добавить в админку или нет
yes_admin = InlineKeyboardButton('Да', callback_data='admin_1') # Если 1 - то да
no_admin = InlineKeyboardButton('Нет', callback_data='admin_2') # Если 2 - то нет

yes_no_admin = InlineKeyboardMarkup().row(yes_admin, no_admin)


